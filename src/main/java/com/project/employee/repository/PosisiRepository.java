package com.project.employee.repository;

import com.project.employee.model.Posisi;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PosisiRepository extends JpaRepository<Posisi,Long>{

    
}