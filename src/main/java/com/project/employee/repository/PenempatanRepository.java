package com.project.employee.repository;

import com.project.employee.model.Penempatan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenempatanRepository extends JpaRepository<Penempatan, Long>{

    
}