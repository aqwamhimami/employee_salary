package com.project.employee.repository;

import com.project.employee.model.TunjanganPegawai;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TunjanganPegawaiRepository extends JpaRepository<TunjanganPegawai, Long>{

    
}