package com.project.employee.repository;

import com.project.employee.model.User;
import com.project.employee.model.UserId;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,UserId>{

    
}