package com.project.employee.repository;

import com.project.employee.model.ListKemampuan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListKemampuanRepository extends JpaRepository<ListKemampuan,Long> {

    
}