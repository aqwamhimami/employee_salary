package com.project.employee.repository;

import com.project.employee.model.ParameterPajak;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParameterPajakRepository extends JpaRepository<ParameterPajak,Long>{

    
}