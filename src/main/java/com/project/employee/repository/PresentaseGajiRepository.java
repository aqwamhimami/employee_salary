package com.project.employee.repository;

import com.project.employee.model.PresentaseGaji;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PresentaseGajiRepository extends JpaRepository<PresentaseGaji,Long>{

    
}