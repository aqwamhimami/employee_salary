package com.project.employee.repository;

import com.project.employee.model.Karyawan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KaryawanRepository extends JpaRepository<Karyawan, Long>{

    
}