package com.project.employee.repository;

import com.project.employee.model.Pendapatan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendapatanRepository extends JpaRepository<Pendapatan, Long>{

    
}