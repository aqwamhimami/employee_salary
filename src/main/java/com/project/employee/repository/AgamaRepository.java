package com.project.employee.repository;

import com.project.employee.model.Agama;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Long>{

    
}