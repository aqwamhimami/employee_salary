package com.project.employee.repository;

import com.project.employee.model.Tingkatan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TingkatanRepository extends JpaRepository<Tingkatan,Long>{

    
}