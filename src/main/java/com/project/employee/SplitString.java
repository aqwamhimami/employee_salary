package com.project.employee;

public class SplitString {

	public static void main(String[] args) {
//		String number = "0.123456";
//		String code = "WISE-LIM-000000500";
		String[] codeArray = {"WISE-LIM-000000500","WISE-LIM-000000600","WISE-LIM-000000450","0.123981798","WISE-LIM-000000700"};

		int lastNumber = 0, lastNumberTemp = 0;
		for (String kode : codeArray) {
			if(kode.contains("-")) {
				String[] arrayNumberTicket = kode.split("-");
				String stringNumber = arrayNumberTicket[2];
				
				lastNumberTemp = Integer.valueOf(stringNumber);
				
				if (lastNumberTemp > lastNumber) {
					lastNumber = lastNumberTemp;
				}
			}
			else {
				lastNumber = lastNumberTemp;
			}
		}

		System.out.println("lastNumber : " + lastNumber);
		System.out.println("lastNumberT: " + lastNumberTemp);
	}
//code.matches("^[a-zA-Z]*$")
}