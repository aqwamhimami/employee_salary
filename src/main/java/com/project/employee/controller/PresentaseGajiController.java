package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.PresentaseGaji;
import com.project.employee.modelDTO.PresentaseGajiDTO;
import com.project.employee.repository.PresentaseGajiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/presentaseGaji")
public class PresentaseGajiController {

    @Autowired
    PresentaseGajiRepository presentaseGajiRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<PresentaseGaji> listPresentaseGajiEntity = presentaseGajiRepository.findAll();
        List<PresentaseGajiDTO> listDTOPresentaseGaji = new ArrayList<PresentaseGajiDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (PresentaseGaji presentaseGajiEntity : listPresentaseGajiEntity) {
            PresentaseGajiDTO presentaseGajiDTO = new PresentaseGajiDTO();
            presentaseGajiDTO = modelMapper.map(presentaseGajiEntity, PresentaseGajiDTO.class);

            listDTOPresentaseGaji.add(presentaseGajiDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOPresentaseGaji);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        PresentaseGaji presentaseGajiEntity = new PresentaseGaji();

        presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
        presentaseGajiRepository.save(presentaseGajiEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", presentaseGajiDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOnePresentaseGajiById(@PathVariable(value = "id") Long presentaseGajiId){
        Map<String, Object> result = new HashMap<String, Object>();
        PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(presentaseGajiId).get();
        PresentaseGajiDTO presentaseGajiDTO  = new PresentaseGajiDTO();
        
        presentaseGajiDTO = modelMapper.map(presentaseGajiEntity, PresentaseGajiDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", presentaseGajiDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updatePresentaseGaji(@PathVariable(value = "id") Long presentaseGajiId,
                                            @Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(presentaseGajiId).get();
        
        presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
        presentaseGajiEntity.setIdPresentaseGaji(presentaseGajiId);

        presentaseGajiRepository.save(presentaseGajiEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", presentaseGajiDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long presentaseGajiId) {
        PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(presentaseGajiId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        presentaseGajiRepository.delete(presentaseGaji);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}