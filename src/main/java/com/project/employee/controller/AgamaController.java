package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Agama;
import com.project.employee.modelDTO.AgamaDTO;
import com.project.employee.repository.AgamaRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/agama")
public class AgamaController {

    @Autowired
    AgamaRepository agamaRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @CrossOrigin(allowedHeaders = "206.189.90.176:8443")
//    @RequestMapping(method = RequestMethod.GET)
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Agama> listAgamaEntity = agamaRepository.findAll();
        List<AgamaDTO> listDTOAgama = new ArrayList<AgamaDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Agama agamaEntity : listAgamaEntity) {
            AgamaDTO agamaDTO = new AgamaDTO();
            agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);

            listDTOAgama.add(agamaDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOAgama);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody AgamaDTO agamaDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Agama agamaEntity = new Agama();

        agamaEntity = modelMapper.map(agamaDTO, Agama.class);
        agamaRepository.save(agamaEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", agamaDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneAgamaById(@PathVariable(value = "id") Long agamaId){
        Map<String, Object> result = new HashMap<String, Object>();
        Agama agamaEntity = agamaRepository.findById(agamaId).get();
        AgamaDTO agamaDTO  = new AgamaDTO();
        
        agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", agamaDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateAgama(@PathVariable(value = "id") Long agamaId,
                                            @Valid @RequestBody AgamaDTO agamaDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Agama agamaEntity = agamaRepository.findById(agamaId).get();
        
        agamaEntity = modelMapper.map(agamaDTO, Agama.class);
        agamaEntity.setIdAgama(agamaId);

        agamaRepository.save(agamaEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", agamaDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long agamaId) {
        Agama agama = agamaRepository.findById(agamaId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        agamaRepository.delete(agama);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}