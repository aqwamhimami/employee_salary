package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.ListKemampuan;
import com.project.employee.modelDTO.ListKemampuanDTO;
import com.project.employee.repository.ListKemampuanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/listKemampuan")
public class ListKemampuanController {

    @Autowired
    ListKemampuanRepository listKemampuanRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<ListKemampuan> listListKemampuanEntity = listKemampuanRepository.findAll();
        List<ListKemampuanDTO> listDTOListKemampuan = new ArrayList<ListKemampuanDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (ListKemampuan listKemampuanEntity : listListKemampuanEntity) {
            ListKemampuanDTO listKemampuanDTO = new ListKemampuanDTO();
            listKemampuanDTO = modelMapper.map(listKemampuanEntity, ListKemampuanDTO.class);

            listDTOListKemampuan.add(listKemampuanDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOListKemampuan);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody ListKemampuanDTO listKemampuanDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        ListKemampuan listKemampuanEntity = new ListKemampuan();

        listKemampuanEntity = modelMapper.map(listKemampuanDTO, ListKemampuan.class);
        listKemampuanRepository.save(listKemampuanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", listKemampuanDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneListKemampuanById(@PathVariable(value = "id") Long listKemampuanId){
        Map<String, Object> result = new HashMap<String, Object>();
        ListKemampuan listKemampuanEntity = listKemampuanRepository.findById(listKemampuanId).get();
        ListKemampuanDTO listKemampuanDTO  = new ListKemampuanDTO();
        
        listKemampuanDTO = modelMapper.map(listKemampuanEntity, ListKemampuanDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", listKemampuanDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateListKemampuan(@PathVariable(value = "id") Long listKemampuanId,
                                            @Valid @RequestBody ListKemampuanDTO listKemampuanDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        ListKemampuan listKemampuanEntity = listKemampuanRepository.findById(listKemampuanId).get();
        
        listKemampuanEntity = modelMapper.map(listKemampuanDTO, ListKemampuan.class);
        listKemampuanEntity.setIdListKemampuan(listKemampuanId);

        listKemampuanRepository.save(listKemampuanEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", listKemampuanDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long listKemampuanId) {
        ListKemampuan listKemampuan = listKemampuanRepository.findById(listKemampuanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        listKemampuanRepository.delete(listKemampuan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}