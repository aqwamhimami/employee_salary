package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.ParameterPajak;
import com.project.employee.modelDTO.ParameterPajakDTO;
import com.project.employee.repository.ParameterPajakRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/parameterPajak")
public class ParameterPajakController {

    @Autowired
    ParameterPajakRepository parameterPajakRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<ParameterPajak> listParameterPajakEntity = parameterPajakRepository.findAll();
        List<ParameterPajakDTO> listDTOParameterPajak = new ArrayList<ParameterPajakDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (ParameterPajak parameterPajakEntity : listParameterPajakEntity) {
            ParameterPajakDTO parameterPajakDTO = new ParameterPajakDTO();
            parameterPajakDTO = modelMapper.map(parameterPajakEntity, ParameterPajakDTO.class);

            listDTOParameterPajak.add(parameterPajakDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOParameterPajak);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody ParameterPajakDTO parameterPajakDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        ParameterPajak parameterPajakEntity = new ParameterPajak();

        parameterPajakEntity = modelMapper.map(parameterPajakDTO, ParameterPajak.class);
        parameterPajakRepository.save(parameterPajakEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", parameterPajakDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneParameterPajakById(@PathVariable(value = "id") Long parameterPajakId){
        Map<String, Object> result = new HashMap<String, Object>();
        ParameterPajak parameterPajakEntity = parameterPajakRepository.findById(parameterPajakId).get();
        ParameterPajakDTO parameterPajakDTO  = new ParameterPajakDTO();
        
        parameterPajakDTO = modelMapper.map(parameterPajakEntity, ParameterPajakDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", parameterPajakDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateParameterPajak(@PathVariable(value = "id") Long parameterPajakId,
                                            @Valid @RequestBody ParameterPajakDTO parameterPajakDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        ParameterPajak parameterPajakEntity = parameterPajakRepository.findById(parameterPajakId).get();
        
        parameterPajakEntity = modelMapper.map(parameterPajakDTO, ParameterPajak.class);
        parameterPajakEntity.setIdParamPajak(parameterPajakId);

        parameterPajakRepository.save(parameterPajakEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", parameterPajakDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long parameterPajakId) {
        ParameterPajak parameterPajak = parameterPajakRepository.findById(parameterPajakId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        parameterPajakRepository.delete(parameterPajak);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}