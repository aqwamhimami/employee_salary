package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.KategoriKemampuan;
import com.project.employee.modelDTO.KategoriKemampuanDTO;
import com.project.employee.repository.KategoriKemampuanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/kategori_kemampuan")
public class KategoriKemampuanController {

    @Autowired
    KategoriKemampuanRepository kategoriKemampuanRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<KategoriKemampuan> listKategoriKemampuanEntity = kategoriKemampuanRepository.findAll();
        List<KategoriKemampuanDTO> listDTOKategoriKemampuan = new ArrayList<KategoriKemampuanDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (KategoriKemampuan kategoriKemampuanEntity : listKategoriKemampuanEntity) {
            KategoriKemampuanDTO kategoriKemampuanDTO = new KategoriKemampuanDTO();
            kategoriKemampuanDTO = modelMapper.map(kategoriKemampuanEntity, KategoriKemampuanDTO.class);

            listDTOKategoriKemampuan.add(kategoriKemampuanDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOKategoriKemampuan);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody KategoriKemampuanDTO kategoriKemampuanDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        KategoriKemampuan kategoriKemampuanEntity = new KategoriKemampuan();

        kategoriKemampuanEntity = modelMapper.map(kategoriKemampuanDTO, KategoriKemampuan.class);
        kategoriKemampuanRepository.save(kategoriKemampuanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", kategoriKemampuanDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneKategoriKemampuanById(@PathVariable(value = "id") Long kategoriKemampuanId){
        Map<String, Object> result = new HashMap<String, Object>();
        KategoriKemampuan kategoriKemampuanEntity = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();
        KategoriKemampuanDTO kategoriKemampuanDTO  = new KategoriKemampuanDTO();
        
        kategoriKemampuanDTO = modelMapper.map(kategoriKemampuanEntity, KategoriKemampuanDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", kategoriKemampuanDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateKategoriKemampuan(@PathVariable(value = "id") Long kategoriKemampuanId,
                                            @Valid @RequestBody KategoriKemampuanDTO kategoriKemampuanDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        KategoriKemampuan kategoriKemampuanEntity = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();
        
        kategoriKemampuanEntity = modelMapper.map(kategoriKemampuanDTO, KategoriKemampuan.class);
        kategoriKemampuanEntity.setIdKategori(kategoriKemampuanId);

        kategoriKemampuanRepository.save(kategoriKemampuanEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", kategoriKemampuanDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long kategoriKemampuanId) {
        KategoriKemampuan kategoriKemampuan = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        kategoriKemampuanRepository.delete(kategoriKemampuan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}