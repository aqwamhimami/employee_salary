package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Parameter;
import com.project.employee.modelDTO.ParameterDTO;
import com.project.employee.repository.ParameterRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/parameter")
public class ParameterController {

    @Autowired
    ParameterRepository parameterRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Parameter> listParameterEntity = parameterRepository.findAll();
        List<ParameterDTO> listDTOParameter = new ArrayList<ParameterDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Parameter parameterEntity : listParameterEntity) {
            ParameterDTO parameterDTO = new ParameterDTO();
            parameterDTO = modelMapper.map(parameterEntity, ParameterDTO.class);

            listDTOParameter.add(parameterDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOParameter);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody ParameterDTO parameterDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Parameter parameterEntity = new Parameter();

        parameterEntity = modelMapper.map(parameterDTO, Parameter.class);
        parameterRepository.save(parameterEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", parameterDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneParameterById(@PathVariable(value = "id") Long parameterId){
        Map<String, Object> result = new HashMap<String, Object>();
        Parameter parameterEntity = parameterRepository.findById(parameterId).get();
        ParameterDTO parameterDTO  = new ParameterDTO();
        
        parameterDTO = modelMapper.map(parameterEntity, ParameterDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", parameterDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateParameter(@PathVariable(value = "id") Long parameterId,
                                            @Valid @RequestBody ParameterDTO parameterDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Parameter parameterEntity = parameterRepository.findById(parameterId).get();
        
        parameterEntity = modelMapper.map(parameterDTO, Parameter.class);
        parameterEntity.setIdParam(parameterId);

        parameterRepository.save(parameterEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", parameterDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long parameterId) {
        Parameter parameter = parameterRepository.findById(parameterId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        parameterRepository.delete(parameter);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}