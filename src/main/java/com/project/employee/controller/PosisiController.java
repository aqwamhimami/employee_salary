package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Posisi;
import com.project.employee.modelDTO.PosisiDTO;
import com.project.employee.repository.PosisiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/posisi")
public class PosisiController {

    @Autowired
    PosisiRepository posisiRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Posisi> listPosisiEntity = posisiRepository.findAll();
        List<PosisiDTO> listDTOPosisi = new ArrayList<PosisiDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Posisi posisiEntity : listPosisiEntity) {
            PosisiDTO posisiDTO = new PosisiDTO();
            posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);

            listDTOPosisi.add(posisiDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOPosisi);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody PosisiDTO posisiDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Posisi posisiEntity = new Posisi();

        posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
        posisiRepository.save(posisiEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", posisiDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOnePosisiById(@PathVariable(value = "id") Long posisiId){
        Map<String, Object> result = new HashMap<String, Object>();
        Posisi posisiEntity = posisiRepository.findById(posisiId).get();
        PosisiDTO posisiDTO  = new PosisiDTO();
        
        posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", posisiDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updatePosisi(@PathVariable(value = "id") Long posisiId,
                                            @Valid @RequestBody PosisiDTO posisiDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Posisi posisiEntity = posisiRepository.findById(posisiId).get();
        
        posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
        posisiEntity.setIdPosisi(posisiId);

        posisiRepository.save(posisiEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", posisiDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long posisiId) {
        Posisi posisi = posisiRepository.findById(posisiId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        posisiRepository.delete(posisi);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}