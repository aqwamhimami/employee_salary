package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Kemampuan;
import com.project.employee.modelDTO.KemampuanDTO;
import com.project.employee.repository.KemampuanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/kemampuan")
public class KemampuanController {

    @Autowired
    KemampuanRepository kemampuanRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Kemampuan> listKemampuanEntity = kemampuanRepository.findAll();
        List<KemampuanDTO> listDTOKemampuan = new ArrayList<KemampuanDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Kemampuan kemampuanEntity : listKemampuanEntity) {
            KemampuanDTO kemampuanDTO = new KemampuanDTO();
            kemampuanDTO = modelMapper.map(kemampuanEntity, KemampuanDTO.class);

            listDTOKemampuan.add(kemampuanDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOKemampuan);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody KemampuanDTO kemampuanDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Kemampuan kemampuanEntity = new Kemampuan();

        kemampuanEntity = modelMapper.map(kemampuanDTO, Kemampuan.class);
        kemampuanRepository.save(kemampuanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", kemampuanDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneKemampuanById(@PathVariable(value = "id") Long kemampuanId){
        Map<String, Object> result = new HashMap<String, Object>();
        Kemampuan kemampuanEntity = kemampuanRepository.findById(kemampuanId).get();
        KemampuanDTO kemampuanDTO  = new KemampuanDTO();
        
        kemampuanDTO = modelMapper.map(kemampuanEntity, KemampuanDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", kemampuanDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateKemampuan(@PathVariable(value = "id") Long kemampuanId,
                                            @Valid @RequestBody KemampuanDTO kemampuanDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Kemampuan kemampuanEntity = kemampuanRepository.findById(kemampuanId).get();
        
        kemampuanEntity = modelMapper.map(kemampuanDTO, Kemampuan.class);
        kemampuanEntity.setIdKemampuan(kemampuanId);

        kemampuanRepository.save(kemampuanEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", kemampuanDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long kemampuanId) {
        Kemampuan kemampuan = kemampuanRepository.findById(kemampuanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        kemampuanRepository.delete(kemampuan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}