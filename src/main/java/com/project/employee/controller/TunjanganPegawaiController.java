package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.TunjanganPegawai;
import com.project.employee.modelDTO.TunjanganPegawaiDTO;
import com.project.employee.repository.TunjanganPegawaiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tunjanganPegawai")
public class TunjanganPegawaiController {

    @Autowired
    TunjanganPegawaiRepository tunjanganPegawaiRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<TunjanganPegawai> listTunjanganPegawaiEntity = tunjanganPegawaiRepository.findAll();
        List<TunjanganPegawaiDTO> listDTOTunjanganPegawai = new ArrayList<TunjanganPegawaiDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (TunjanganPegawai tunjanganPegawaiEntity : listTunjanganPegawaiEntity) {
            TunjanganPegawaiDTO tunjanganPegawaiDTO = new TunjanganPegawaiDTO();
            tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawaiEntity, TunjanganPegawaiDTO.class);

            listDTOTunjanganPegawai.add(tunjanganPegawaiDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOTunjanganPegawai);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        TunjanganPegawai tunjanganPegawaiEntity = new TunjanganPegawai();

        tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
        tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", tunjanganPegawaiDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneTunjanganPegawaiById(@PathVariable(value = "id") Long tunjanganPegawaiId){
        Map<String, Object> result = new HashMap<String, Object>();
        TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();
        TunjanganPegawaiDTO tunjanganPegawaiDTO  = new TunjanganPegawaiDTO();
        
        tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawaiEntity, TunjanganPegawaiDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", tunjanganPegawaiDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateTunjanganPegawai(@PathVariable(value = "id") Long tunjanganPegawaiId,
                                            @Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();
        
        tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
        tunjanganPegawaiEntity.setIdTunjanganPegawai(tunjanganPegawaiId);

        tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", tunjanganPegawaiDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long tunjanganPegawaiId) {
        TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        tunjanganPegawaiRepository.delete(tunjanganPegawai);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}