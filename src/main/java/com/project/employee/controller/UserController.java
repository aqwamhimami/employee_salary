// package com.project.employee.controller;

// import java.util.ArrayList;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;

// import javax.validation.Valid;

// import com.project.employee.model.User;
// import com.project.employee.modelDTO.UserDTO;
// import com.project.employee.repository.UserRepository;

// import org.modelmapper.ModelMapper;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.DeleteMapping;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.PutMapping;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;

// @RestController
// @RequestMapping("/api/user")
// public class UserController {

//     @Autowired
//     UserRepository userRepository;
//     ModelMapper modelMapper = new ModelMapper();

//     // GET ALL
//     @GetMapping("/readAll")
//     public Map<String, Object> getReadAll(){
//         List<User> listUserEntity = userRepository.findAll();
//         List<UserDTO> listDTOUser = new ArrayList<UserDTO>();
//         Map<String, Object> result = new HashMap<String,Object>();

//         for (User userEntity : listUserEntity) {
//             UserDTO userDTO = new UserDTO();
//             userDTO = modelMapper.map(userEntity, UserDTO.class);

//             listDTOUser.add(userDTO);
//         }

//         result.put("Status", 200);
//         result.put("message", "Read All Data Success");
//         result.put("Data", listDTOUser);

//         return result;
//     } 

//     // ADD NEW
//     @PostMapping("/create")
//     public Map<String, Object> createDTO(@Valid @RequestBody UserDTO userDTO){
//         Map<String, Object> result = new HashMap<String,Object>();
//         User userEntity = new User();

//         userEntity = modelMapper.map(userDTO, User.class);
//         userRepository.save(userEntity);
        
//         result.put("Status", 200);
//         result.put("message", "Create Data Success");
//         result.put("Data", userDTO);

//         return result;
//     }
    
//     // GET ONE
//     @GetMapping("/getOne/{id}")
//     public Map<String,Object> getOneUserById(@PathVariable(value = "id") Long userId){
//         Map<String, Object> result = new HashMap<String, Object>();
//         User userEntity = userRepository.findById(userId).get();
//         UserDTO userDTO  = new UserDTO();
        
//         userDTO = modelMapper.map(userEntity, UserDTO.class);

//         result.put("Status", 200);
//         result.put("message", "Read One Data Success");
//         result.put("Data", userDTO);
        
//         return result;
//     }

//     // UPDATE
//     @PutMapping("/update/{id}")
//     public Map<String, Object> updateUser(@PathVariable(value = "id") Long userId,
//                                             @Valid @RequestBody UserDTO userDTO) {
//         Map<String, Object> result = new HashMap<String, Object>();
//         User userEntity = userRepository.findById(userId).get();
        
//         userEntity = modelMapper.map(userDTO, User.class);
//         userEntity.setIdUser(userId);

//         userRepository.save(userEntity);

//         result.put("Status", 200);
//         result.put("message", "Update Data Success");
//         result.put("Data", userDTO);

//         return result;
//     }

//     @DeleteMapping("/delete/{id}")
//     public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long userId) {
//         User user = userRepository.findById(userId).get();
//         Map<String,Object> result = new HashMap<String,Object>();
//         userRepository.delete(user);

//         result.put("Status", 200);
//         result.put("message", "Delete Data Success");

//         return result;
//     }

// }