package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Pendapatan;
import com.project.employee.modelDTO.PendapatanDTO;
import com.project.employee.repository.PendapatanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pendapatan")
public class PendapatanController {

    @Autowired
    PendapatanRepository pendapatanRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Pendapatan> listPendapatanEntity = pendapatanRepository.findAll();
        List<PendapatanDTO> listDTOPendapatan = new ArrayList<PendapatanDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Pendapatan pendapatanEntity : listPendapatanEntity) {
            PendapatanDTO pendapatanDTO = new PendapatanDTO();
            pendapatanDTO = modelMapper.map(pendapatanEntity, PendapatanDTO.class);

            listDTOPendapatan.add(pendapatanDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOPendapatan);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody PendapatanDTO pendapatanDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Pendapatan pendapatanEntity = new Pendapatan();

        pendapatanEntity = modelMapper.map(pendapatanDTO, Pendapatan.class);
        pendapatanRepository.save(pendapatanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", pendapatanDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOnePendapatanById(@PathVariable(value = "id") Long pendapatanId){
        Map<String, Object> result = new HashMap<String, Object>();
        Pendapatan pendapatanEntity = pendapatanRepository.findById(pendapatanId).get();
        PendapatanDTO pendapatanDTO  = new PendapatanDTO();
        
        pendapatanDTO = modelMapper.map(pendapatanEntity, PendapatanDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", pendapatanDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updatePendapatan(@PathVariable(value = "id") Long pendapatanId,
                                            @Valid @RequestBody PendapatanDTO pendapatanDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Pendapatan pendapatanEntity = pendapatanRepository.findById(pendapatanId).get();
        
        pendapatanEntity = modelMapper.map(pendapatanDTO, Pendapatan.class);
        pendapatanEntity.setIdPendapatan(pendapatanId);

        pendapatanRepository.save(pendapatanEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", pendapatanDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long pendapatanId) {
        Pendapatan pendapatan = pendapatanRepository.findById(pendapatanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        pendapatanRepository.delete(pendapatan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}