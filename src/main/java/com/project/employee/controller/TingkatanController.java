package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Tingkatan;
import com.project.employee.modelDTO.TingkatanDTO;
import com.project.employee.repository.TingkatanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tingkatan")
public class TingkatanController {

    @Autowired
    TingkatanRepository tingkatanRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Tingkatan> listTingkatanEntity = tingkatanRepository.findAll();
        List<TingkatanDTO> listDTOTingkatan = new ArrayList<TingkatanDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Tingkatan tingkatanEntity : listTingkatanEntity) {
            TingkatanDTO tingkatanDTO = new TingkatanDTO();
            tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);

            listDTOTingkatan.add(tingkatanDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOTingkatan);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody TingkatanDTO tingkatanDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Tingkatan tingkatanEntity = new Tingkatan();

        tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
        tingkatanRepository.save(tingkatanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", tingkatanDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneTingkatanById(@PathVariable(value = "id") Long tingkatanId){
        Map<String, Object> result = new HashMap<String, Object>();
        Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanId).get();
        TingkatanDTO tingkatanDTO  = new TingkatanDTO();
        
        tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", tingkatanDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateTingkatan(@PathVariable(value = "id") Long tingkatanId,
                                            @Valid @RequestBody TingkatanDTO tingkatanDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanId).get();
        
        tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
        tingkatanEntity.setIdTingkatan(tingkatanId);

        tingkatanRepository.save(tingkatanEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", tingkatanDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long tingkatanId) {
        Tingkatan tingkatan = tingkatanRepository.findById(tingkatanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        tingkatanRepository.delete(tingkatan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}