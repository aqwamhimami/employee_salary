package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Karyawan;
import com.project.employee.modelDTO.KaryawanDTO;
import com.project.employee.repository.KaryawanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/karyawan")
public class KaryawanController {

    @Autowired
    KaryawanRepository karyawanRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Karyawan> listKaryawanEntity = karyawanRepository.findAll();
        List<KaryawanDTO> listDTOKaryawan = new ArrayList<KaryawanDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Karyawan karyawanEntity : listKaryawanEntity) {
            KaryawanDTO karyawanDTO = new KaryawanDTO();
            karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);

            listDTOKaryawan.add(karyawanDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOKaryawan);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody KaryawanDTO karyawanDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Karyawan karyawanEntity = new Karyawan();

        karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
        karyawanRepository.save(karyawanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", karyawanDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOneKaryawanById(@PathVariable(value = "id") Long karyawanId){
        Map<String, Object> result = new HashMap<String, Object>();
        Karyawan karyawanEntity = karyawanRepository.findById(karyawanId).get();
        KaryawanDTO karyawanDTO  = new KaryawanDTO();
        
        karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", karyawanDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updateKaryawan(@PathVariable(value = "id") Long karyawanId,
                                            @Valid @RequestBody KaryawanDTO karyawanDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Karyawan karyawanEntity = karyawanRepository.findById(karyawanId).get();
        
        karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
        karyawanEntity.setIdKaryawan(karyawanId);

        karyawanRepository.save(karyawanEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", karyawanDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long karyawanId) {
        Karyawan karyawan = karyawanRepository.findById(karyawanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        karyawanRepository.delete(karyawan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}