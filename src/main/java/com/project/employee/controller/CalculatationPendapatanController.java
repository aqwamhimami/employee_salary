package com.project.employee.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;

import com.project.employee.model.Karyawan;
import com.project.employee.model.Parameter;
import com.project.employee.model.Pendapatan;
import com.project.employee.model.Posisi;
import com.project.employee.model.PresentaseGaji;
import com.project.employee.model.TunjanganPegawai;
import com.project.employee.repository.KaryawanRepository;
import com.project.employee.repository.ParameterRepository;
import com.project.employee.repository.PendapatanRepository;
import com.project.employee.repository.PenempatanRepository;
import com.project.employee.repository.PresentaseGajiRepository;
import com.project.employee.repository.TunjanganPegawaiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/calculationPendapatan")
public class CalculatationPendapatanController {
    
    @Autowired
    PendapatanRepository pendapatanRepository;
    @Autowired
    KaryawanRepository karyawanRepository;
    @Autowired
    PenempatanRepository penempatanRepository;
    @Autowired
    PresentaseGajiRepository presentaseGajiRepository;
    @Autowired
    TunjanganPegawaiRepository tunjanganPegawaiRepository;
    @Autowired
    ParameterRepository parameterRepository;

    ModelMapper modelMapper = new ModelMapper();

    @PostMapping("/update/{tanggalGajian}")
    public Map<String, Object> pendapatan(@PathVariable(value = "tanggalGajian") String tanggalGajian) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<Karyawan> listKaryawan = karyawanRepository.findAll();
        List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll();
        List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
        Parameter parameter = parameterRepository.findById((long)1).get();
        BigDecimal skip = BigDecimal.valueOf(0), gajiPokok = null, tunjanganKeluarga = null, tunjanganTransport = null, tunjanganPegawai = null, gajiKotor = null, PPH = null, bpjs = null, gajiBersih = null, uangLembur = null, uangBonus = null, takeHomePay = null;

        for (Karyawan karyawan : listKaryawan) {
            Pendapatan pendapatan = new Pendapatan();
            int masaKerjaTemp = 0;
            BigDecimal persentaseGajiTemp = null;
            BigDecimal umk = karyawan.getPenempatan().getUmkPenempatan();
            System.out.println("+++++++++++++++++++++++++++++++++++");
            System.out.println("nama      : " + karyawan.getNama());
            System.out.println("posisi    : " + karyawan.getPosisi().getNamaPosisi());
            System.out.println("tingkat   : " + karyawan.getTingkatan().getNamaTingkatan());
            System.out.println("masakerja : " + karyawan.getMasaKerja());

            List<Integer> listMasaKerja = new ArrayList<Integer>(3);
            int index = 0;
            for (PresentaseGaji presentaseGaji : listPresentaseGaji) {
                
                if (karyawan.getPosisi().getIdPosisi() == presentaseGaji.getPosisi().getIdPosisi() && 
                    karyawan.getTingkatan().getIdTingkatan() == presentaseGaji.getIdTingkatan()) {

                    listMasaKerja.add(presentaseGaji.getMasaKerja());

                    if (karyawan.getMasaKerja() <= listMasaKerja.get(index) && masaKerjaTemp == 0) {
                        masaKerjaTemp = listMasaKerja.get(index);
                        persentaseGajiTemp = presentaseGaji.getBesaranGaji();
                    }
                    else if(karyawan.getMasaKerja() >= listMasaKerja.get(index) && index == 2){
                        masaKerjaTemp = listMasaKerja.get(index);
                        persentaseGajiTemp = presentaseGaji.getBesaranGaji();
                    }
                    index++;
                }
                
            }

            gajiPokok = gajiPokokCalculation(umk, persentaseGajiTemp);
            tunjanganKeluarga = tunjanganKeluargaCalculation(gajiPokok, karyawan.getStatusPernikahan());
            tunjanganTransport = tunjanganTransportCalculation(karyawan.getPenempatan().getKotaPenempatan(), parameter);
            tunjanganPegawai = tunjanganPegawaiCalculation(karyawan, listTunjanganPegawai);
            gajiKotor = gajiKotorCalculation(gajiPokok, tunjanganKeluarga, tunjanganPegawai, tunjanganTransport);
            PPH = skip;
            bpjs = bpjsCalulation(gajiPokok, parameter);
            gajiBersih = gajiBersihCalculation(gajiKotor, bpjs, PPH);
            uangLembur = skip;
            uangBonus = bonusCalculation(parameter, karyawan.getPosisi());
            takeHomePay = takeHomePayCalculation(gajiBersih, uangLembur, uangBonus);

            LocalDate myDate =LocalDate.parse(tanggalGajian);
            Date date = Date.from(myDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            
            System.out.println(myDate);
            System.out.println(date);

            System.out.println("Temp Masa Kerja : " + masaKerjaTemp);
            System.out.println("gaji pokok   : " + gajiPokok);
            System.out.println("Tunjangan kel: " + tunjanganKeluarga);
            System.out.println("Tunjangan trn: " + tunjanganTransport);
            System.out.println("Tunjangan pgw: " + tunjanganPegawai);
            System.out.println("Gaji Kotor   : " + gajiKotor);
            System.out.println("PPH          : " + PPH);
            System.out.println("BPJS         : " + bpjs);
            System.out.println("Gaji Bersih  : " + gajiBersih);
            System.out.println("Uang Lembur  : " + uangLembur);
            System.out.println("Uang Bonus   : " + uangBonus);
            System.out.println("Take Home Pay: " + takeHomePay);

            pendapatan.setGajiPokok(gajiPokok);
            pendapatan.setKaryawan(karyawan);
            pendapatan.setTanggalGaji(date);
            pendapatan.setTunjanganKeluarga(tunjanganKeluarga);
            pendapatan.setTunjanganTransport(tunjanganTransport);
            pendapatan.setTunjanganPegawai(tunjanganPegawai);
            pendapatan.setGajiKotor(gajiKotor);
            pendapatan.setPphPerbulan(PPH);
            pendapatan.setBpjs(bpjs);
            pendapatan.setGajiBersih(gajiBersih);
            pendapatan.setUangLembur(uangLembur);
            pendapatan.setUangBonus(uangBonus);
            pendapatan.setTakeHomePay(takeHomePay);

            pendapatanRepository.save(pendapatan);
        }

        result.put("Result", "Calculation Gaji Pokok Success");

        return result;
    } 

    public BigDecimal gajiPokokCalculation(BigDecimal umk, BigDecimal percentaseBesaranGaji) { 

        double gajiPokok = umk.doubleValue() * percentaseBesaranGaji.doubleValue();
        
        return BigDecimal.valueOf(gajiPokok);
    }

    public BigDecimal tunjanganKeluargaCalculation(BigDecimal gajiPokok, Short statusPernikahan){
        double tunjanganKeluarga;
        if (statusPernikahan == 1) {
            tunjanganKeluarga = 0.02 * gajiPokok.doubleValue();
        } else {
            tunjanganKeluarga = 0;
        }

        return BigDecimal.valueOf(tunjanganKeluarga);
    }

    public BigDecimal tunjanganTransportCalculation(String penempatan, Parameter parameter){
        String cityForBonusTransport = "jakarta";
        double tunjanganTransport;
        if (penempatan.equalsIgnoreCase(cityForBonusTransport)) {
            tunjanganTransport = parameter.getTTransport().doubleValue();
        }
        else{
            tunjanganTransport = 0;
        }

        return BigDecimal.valueOf(tunjanganTransport);
    }

    public BigDecimal tunjanganPegawaiCalculation(Karyawan karyawan, List<TunjanganPegawai> listTunjanganPegawai){
        BigDecimal tunjangan = BigDecimal.valueOf(0);
        for (TunjanganPegawai tunjanganPegawai : listTunjanganPegawai) {
            if (karyawan.getTingkatan() == tunjanganPegawai.getTingkatan() && karyawan.getPosisi() == tunjanganPegawai.getPosisi()) {
                tunjangan = tunjanganPegawai.getBesaranTujnaganPegawai();
            }
        }
        return tunjangan;
    }

    public BigDecimal gajiKotorCalculation(BigDecimal gajiPokok, BigDecimal tunjanganKeluarga, BigDecimal tunjanganPegawai, BigDecimal tunjanganTransport){
        double gajiKotor = gajiPokok.doubleValue() + tunjanganKeluarga.doubleValue() + tunjanganPegawai.doubleValue() + tunjanganTransport.doubleValue();

        return BigDecimal.valueOf(gajiKotor);
    }

    public BigDecimal bpjsCalulation(BigDecimal gajiPokok, Parameter parameter){
        double bpjs = gajiPokok.doubleValue() * parameter.getPBpjs().doubleValue();
        return BigDecimal.valueOf(bpjs);
    }

    public BigDecimal gajiBersihCalculation(BigDecimal gajiKotor, BigDecimal bpjs, BigDecimal pph){
        double gajiBersih = gajiKotor.doubleValue() - bpjs.doubleValue() - pph.doubleValue();
        return BigDecimal.valueOf(gajiBersih);
    }

    public BigDecimal bonusCalculation(Parameter parameter, Posisi posisi){
        double bonus = 0;
        if (posisi.getNamaPosisi().equalsIgnoreCase("Programmer")) {
            bonus = parameter.getBonusPg().doubleValue();
        } else if (posisi.getNamaPosisi().equalsIgnoreCase("Tester")) {
            bonus = parameter.getBonusTs().doubleValue();
        } else if (posisi.getNamaPosisi().equalsIgnoreCase("Technical Writter")){
            bonus = parameter.getBonusTw().doubleValue();
        }
        return BigDecimal.valueOf(bonus);
    }

    public BigDecimal takeHomePayCalculation(BigDecimal gajiBersih, BigDecimal uangLembur, BigDecimal uangBonus){
        double takeHomePay = gajiBersih.doubleValue() + uangLembur.doubleValue() + uangBonus.doubleValue();
        return BigDecimal.valueOf(takeHomePay);
    }

}