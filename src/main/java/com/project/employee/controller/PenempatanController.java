package com.project.employee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.project.employee.model.Penempatan;
import com.project.employee.modelDTO.PenempatanDTO;
import com.project.employee.repository.PenempatanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/penempatan")
public class PenempatanController {

    @Autowired
    PenempatanRepository penempatanRepository;
    ModelMapper modelMapper = new ModelMapper();

    // GET ALL
    @GetMapping("/readAll")
    public Map<String, Object> getReadAll(){
        List<Penempatan> listPenempatanEntity = penempatanRepository.findAll();
        List<PenempatanDTO> listDTOPenempatan = new ArrayList<PenempatanDTO>();
        Map<String, Object> result = new HashMap<String,Object>();

        for (Penempatan penempatanEntity : listPenempatanEntity) {
            PenempatanDTO penempatanDTO = new PenempatanDTO();
            penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);

            listDTOPenempatan.add(penempatanDTO);
        }

        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOPenempatan);

        return result;
    } 

    // ADD NEW
    @PostMapping("/create")
    public Map<String, Object> createDTO(@Valid @RequestBody PenempatanDTO penempatanDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        Penempatan penempatanEntity = new Penempatan();

        penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
        penempatanRepository.save(penempatanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", penempatanDTO);

        return result;
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Map<String,Object> getOnePenempatanById(@PathVariable(value = "id") Long penempatanId){
        Map<String, Object> result = new HashMap<String, Object>();
        Penempatan penempatanEntity = penempatanRepository.findById(penempatanId).get();
        PenempatanDTO penempatanDTO  = new PenempatanDTO();
        
        penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);

        result.put("Status", 200);
        result.put("message", "Read One Data Success");
        result.put("Data", penempatanDTO);
        
        return result;
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Map<String, Object> updatePenempatan(@PathVariable(value = "id") Long penempatanId,
                                            @Valid @RequestBody PenempatanDTO penempatanDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        Penempatan penempatanEntity = penempatanRepository.findById(penempatanId).get();
        
        penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
        penempatanEntity.setIdPenempatan(penempatanId);

        penempatanRepository.save(penempatanEntity);

        result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", penempatanDTO);

        return result;
    }

    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAuthor(@PathVariable(value = "id") Long penempatanId) {
        Penempatan penempatan = penempatanRepository.findById(penempatanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
        penempatanRepository.delete(penempatan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

        return result;
    }

}