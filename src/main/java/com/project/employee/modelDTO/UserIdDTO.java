package com.project.employee.modelDTO;

/**
 * UserIdDTO
 */
public class UserIdDTO {

    private long idUser;
    private String username;
    
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
    
}