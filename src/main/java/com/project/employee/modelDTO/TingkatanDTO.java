package com.project.employee.modelDTO;

/**
 * TingkatanDTO
 */
public class TingkatanDTO {

    private long idTingkatan;
    private String namaTingkatan;
    
	public long getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(long idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public String getNamaTingkatan() {
		return namaTingkatan;
	}
	public void setNamaTingkatan(String namaTingkatan) {
		this.namaTingkatan = namaTingkatan;
	}
}