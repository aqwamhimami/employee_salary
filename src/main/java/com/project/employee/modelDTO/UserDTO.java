package com.project.employee.modelDTO;

/**
 * UserDTO
 */
public class UserDTO {

    private UserIdDTO id;
	private String password;
    private Short status;
    
	public UserIdDTO getId() {
		return id;
	}
	public void setId(UserIdDTO id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}
    

}