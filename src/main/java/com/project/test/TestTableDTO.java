package com.project.test;

public class TestTableDTO {
	
	private long testId;
	private TestBucketDTO testBucket;
	private String nama;
	
	public long getTestId() {
		return testId;
	}
	public void setTestId(long testId) {
		this.testId = testId;
	}
	public TestBucketDTO getTestBucket() {
		return testBucket;
	}
	public void setTestBucket(TestBucketDTO testBucket) {
		this.testBucket = testBucket;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}

}
