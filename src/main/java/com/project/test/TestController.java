package com.project.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.core.data.interfaces.models.IDataUtility;
import com.io.iona.core.enums.OperationMode;
import com.io.iona.springboot.actionflows.custom.CustomBeforeInsert;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;

@RestController
@RequestMapping("/api/test-crud")
public class TestController extends HibernateCRUDController<TestTable, TestTableDTO>/* implements CustomBeforeInsert<TestTable, TestTableDTO>*/{
	
//	@Autowired
//	TestBucketRepository testBucketRepository;
//
//	@Override
//	
//	public void beforeInsert(IDataUtility dataUtility, HibernateDataSource<TestTable, TestTableDTO> dataSource, OperationMode operationMode)
//			throws Exception {
//		TestBucketDTO testBucketDTO = new TestBucketDTO();
//		testBucketDTO.setKelas(dataSource.getDataTransferObject().getNama());
//		TestBucket testBucket = new TestBucket();
//		testBucket.setKelas(testBucketDTO.getKelas());
//		
//		testBucketRepository.save(testBucket);
//		
//		dataSource.getDataTransferObject().setTestBucket(testBucketDTO);
//		
//	}
}
