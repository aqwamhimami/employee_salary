package com.project.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestDesc {

	public static void main(String[] args) {
		Date date = new Date();
		CronUtil crons = new CronUtil(date);
		
		System.out.println("CRON : " + crons);
		

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dt = dateFormat.format(date);

		Date cronDate;
		try {
			cronDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dt);
			CronUtil calHelper = new CronUtil (cronDate);
			String cron = calHelper.getSeconds() + " " +
			calHelper.getMins() + " " +
			calHelper.getHours() + " " +
			calHelper.getDaysOfMonth() + " " +
			calHelper.getMonths() + " " +
			calHelper.getDaysOfWeek() + " " +
			calHelper.getYears() + " ";
			System.out.println("Cron Expression " + cron);

		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
