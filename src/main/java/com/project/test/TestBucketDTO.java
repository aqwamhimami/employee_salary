package com.project.test;

public class TestBucketDTO {

	private long testBucketId;
	private String kelas;
	
	public long getTestBucketId() {
		return testBucketId;
	}
	public void setTestBucketId(long testBucketId) {
		this.testBucketId = testBucketId;
	}
	public String getKelas() {
		return kelas;
	}
	public void setKelas(String kelas) {
		this.kelas = kelas;
	}
	
}
